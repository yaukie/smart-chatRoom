#    <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/奖章.png" width="25px" height="25px" /> smartSocket（分布式聊天室）

##    <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/上午.png" width="25px" height="25px" /> 产品介绍
基于Redis/RocketMQ + SpringBoot + Vue +Websocket 实现简易分布式聊天系统，主要亮点如下：

- 支持基于Redis多通道的订阅发送，按通道实现Session共享
- 支持基于Redis多通道的消息会话隔离，保证消息交流的安全性
- 支持基于Redis多通道消息监控、消息存储，实现离线消息的暂存
- 支持基于RocketMQ中间件的消息发布、订阅，按消息主题区分消息会话
- 支持基于RocketMQ中间件的消息分类处理，实现按标签共享Session、消费Session
- 支持基于RocketMQ中间件的消息离线发送，用户上线按照上次消费位点接收离线消息
- 支持基于RocketMQ中间件的消息监听，处理重复消息，实现消息发布的高效率
- 支持单聊、群聊、广播、按指定通道交流

本系统解决的问题请参考如下：  

  ![订阅模式下架构](./folder/20.png)   
   
   说明如下：   
    1、假设当前系统部署有三个服务器节点，分别是服务器节点1、服务器节点2、服务器节点3；  
    2、服务器节点1分别订阅了通道1、通道2,；服务器节点2只订阅了通道2；服务节点3只订阅了通道1；  
    3、系统发送消息至通道1；  
    4、服务器节点1、服务器节点3收到系统发送到通道1的消息，这两台服务器上的用户Session，可以实现共享；  
    5、服务器节点2没有订阅通道1，该服务器上的用户，收不到消息，查询不到服务器节点1/3上面用户Session信息  
    6、使用 RocketMQ 中间件实现思路跟Redis一样，RocketMQ中间件可以针对主题来订阅不同的消息；
  
##    <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/风景2.png" width="25px" height="25px" /> 解决痛点  
- 解决集群模式下，WebSocket会话无法共享的问题  
  参考如图   
  ![集群模式下架构](./folder/10.png)  
    说明：   
    
    1、集群模式下，客户端通过代理服务器连接服务器节点 ；  
    2、用户A与服务器节点1建立连接，创建Session，存储至当前服务器；  
    3、用户B与服务器节点2建立连接，创建Session，存储至当前服务器；  
    4、用户C与服务器节点3建立连接，创建Session，存储至当前服务器；  
    5、用户A想与用户B交流，用户A无法查询到用户B的Session；同理，用户B、用户C也无法相互访问 ；  
    6、访问不到对方的Session，原因为: 当前集群模式下，Session无法跨服务器节点共享  
    
- 解决高并发场景下，WebSocket会话消息丢失的问题  
- 解决多用户场景下，WebSocket会话无法隔离的问题  

## <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/高科技.png" width="25px" height="25px" /> 软件架构

### 1、前端架构  
目录结构：  

        ├─public     --- 公共文件
        │  └─html
        └─src         --- 核心目录
            ├─api   --- 接口定义区
            │  ├─monitor
            │  ├─system
            │  │  └─dict
            │  └─tool
            ├─assets    --- 静态文件存储区
            │  ├─401_images
            │  ├─404_images
            │  ├─face
            │  ├─head
            │  ├─icons
            │  │  └─svg
            │  ├─images
            │  ├─logo
            │  └─styles
            ├─components     --- 核心组件定义区
            │  ├─Breadcrumb
            │  ├─Crontab
            │  ├─DictData
            │  ├─DictTag
            │  ├─Editor
            │  ├─FileUpload
            │  ├─Hamburger
            │  ├─HeaderSearch
            │  ├─IconSelect
            │  ├─iFrame
            │  ├─ImagePreview
            │  ├─ImageUpload
            │  ├─Pagination
            │  ├─PanThumb
            │  ├─ParentView
            │  ├─RightPanel
            │  ├─RightToolbar
            │  ├─Screenfull
            │  ├─SizeSelect
            │  ├─Smart
            │  │  ├─Doc
            │  │  └─Git
            │  ├─SvgIcon
            │  ├─ThemePicker
            │  └─TopNav
            ├─directive       --- 指令定义区
            │  ├─dialog
            │  ├─module
            │  └─permission
            ├─layout     --- 项目结构定义区
            │  ├─components
            │  │  ├─IframeToggle
            │  │  ├─InnerLink
            │  │  ├─Settings
            │  │  ├─Sidebar
            │  │  └─TagsView
            │  └─mixin
            ├─plugins     --- 插件定义区
            ├─router
            ├─store
            │  └─modules    --- 模块定义区 
            ├─utils
            │  ├─dict
            │  └─generator 
            └─views
                └─error

  ####  1.1 环境
   - 步骤一 安装Node.js    
    版本要 >= v18 ， 用于构建Vue项目，运行时不需要   ，建议使用nvm来统一管理node版本，以下是具体步骤，省去自查麻烦 ：  
   - 步骤二 下载 nvm   
    链接地址： https://nvm.uihtm.com/download.html  
   - 步骤三 双击下载好的 nvm-1.1.12-setup.zip 文件   
   ![nvm1](./folder/nvm3.png)    
   - 步骤四 双击 nvm-setup.exe  开始安装     
      ![nvm1](./folder/nvm4.png)    
   - 步骤五   选择我接受，然后点击next  
       ![nvm1](./folder/nvm5.png)    
   - 步骤六   选择nvm安装路径，路径名称不要有空格，然后点击next  
       ![nvm1](./folder/nvm6.png)    
   - 步骤七   node.js安装路径，然后点击next    
       ![nvm1](./folder/nvm7.png)    
   - 步骤八   点击Install  
       ![nvm1](./folder/nvm8.png)    
   - 步骤九   点击Finish,完成安装  
       ![nvm1](./folder/nvm9.png)    
   - 步骤十   nvm修改镜像源    
        win + R ，输入cmd ，回车后，在命令行输入nvm root 命令，可以查看nvm的安装根路径在那个文件夹
       ![nvm1](./folder/nvm10.png)      
       在文件管理器地址栏输入root地址，找到setting.txt文件并打开    
       ![nvm1](./folder/nvm101.png)      
       复制粘贴以下代码，如图所示。保存完成nvm源修改。  
        node_mirror: https://npmmirror.com/mirrors/node/  
        npm_mirror:  https://npmmirror.com/mirrors/npm/  
          ![nvm1](./folder/nvm102.png)       
          
   - 步骤十一   nvm 安装node    
      通过nvm安装node   
      查看node.js最新的一批版本(版本号：偶数是绝对稳定版本 基数是不一定稳定版本)  
      nvm list available   
     ![nvm1](./folder/nvm11.png)     
         安装指定的版本：nvm install 10.23.0  
         查看安装的所有版本：nvm list 或 nvm ls  
         进入或切换版本：nvm use 18.15.0  
     ![nvm1](./folder/nvm12.png)     
         安装成功后会成对应的文件夹，如下图所示   
      ![nvm1](./folder/nvm13.png)       
      检查是否安装成功： node -v 查看node的版本、npm -v 查看npm的版本  
        ![nvm1](./folder/nvm14.png)       
  - 步骤十二 测试  
        局安装最常用的 express 模块 进行测试  
        npm install express -g  
        完成后会得到一个express的文件  
       ![nvm1](./folder/nvm15.png)  
          ![nvm1](./folder/nvm151.png)    
  - 步骤十三 设置淘宝镜像  
  npm config set registry https://registry.npmmirror.com/  
   
  npm config get registry            
  - 步骤十四 全局安装 cnpm  
  cnpm 本身就是国内的 因此无需配置镜像  
npm install -g cnpm --registry=https://registry.npmmirror.com/  
 
或  
 
npm install -g cnpm  

查看cnpm版本  

cnpm -v  
 
npm list cnpm -g  

   ![nvm1](./folder/nvm16.png)      
   
  #### 1.2 开发  
  
  ```bash
  # 克隆项目
  git clone https://gitee.com/y_project/smart-Vue
  
  # 进入项目目录
  cd smart-chat-ui
  
  # 安装依赖
  npm install
  
  # 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
  npm install --registry=https://registry.npmmirror.com
  
  # 启动服务
  npm run dev
  ```
  
  浏览器访问 http://localhost:80
  
  #### 1.3 发布  
  
  ```bash
  # 构建测试环境
  npm run build:stage
  
  # 构建生产环境
  npm run build:prod
  ```
     
### 2、后端架构  
目录结构：  

    ├─src
    │  ├─main
    │  │  ├─java
    │  │  │  └─com
    │  │  │      └─yaukie
    │  │  │          └─socket
    │  │  │              │  StartApplication.java  --- WebSocket 启动入口
    │  │  │              │  WebSocketServer.java  --- WebSocket 服务核心管理器
    │  │  │              │  
    │  │  │              ├─config   --- WebSocket 核心配置
    │  │  │              │      RedisConfig.java   --- WebSocket 缓存配置管理器
    │  │  │              │      RocketMQTemplateConfig.java   --- WebSocket rocketMQ 管理器
    │  │  │              │      WebSocketConfiguration.java   --- WebSocket 端点管理器
    │  │  │              │      WsSessionConfig.java   --- WebSocket 会话配置管理器
    │  │  │              │      
    │  │  │              ├─constants  --- WebSocket 常量管理器
    │  │  │              │      BaseResult.java
    │  │  │              │      BaseResultConstant.java
    │  │  │              │      Constants.java
    │  │  │              │      
    │  │  │              ├─controller
    │  │  │              │      MessageController.java   --- WebSocket 请求处理器
    │  │  │              │      
    │  │  │              ├─dto
    │  │  │              │      MessageDTO.java
    │  │  │              │      UserDTO.java
    │  │  │              │      
    │  │  │              ├─listener
    │  │  │              │      RedisMessageListener.java   --- WebSocket 通道监听器
    │  │  │              │      
    │  │  │              ├─manager
    │  │  │              │      WsSessionManger.java   --- WebSocket 状态管理器
    │  │  │              │      
    │  │  │              └─rocketmq
    │  │  │                      BaseConsumerListener.java   --- WebSocket mq中间件消息监听器
    │  │  │                      BaseEvent.java   --- WebSocket 基础事件管理器
    │  │  │                      HandlerWsMsg.java   --- WebSocket 消息处理器
    │  │  │                      JackJsonUtil.java   --- WebSocket 工具类
    │  │  │                      MqConstant.java   --- WebSocket MQ 基础参数
    │  │  │                      RocketMqMessage.java   --- WebSocket mq 消息实例
    │  │  │                      TagConstant.java   --- WebSocket mq 标签实例
    │  │  │                      
    │  │  └─resources
    │  │          application.yml   --- WebSocket 核心配置
    │  │          
    │  └─test
    │      ├─java
    │      │  └─com
    │      │      └─lambo
    │      │          └─auth
    │      └─resources
    │          │  jdbc.properties
    │          │  
    │          └─sql
    
 #### 1.1 安装 
   - 步骤一 安装 Redis   
    redis 安装老生长谈了，就不再赘述，简单说明如下：  
    
    Windows系统下安装Redis   
    
    a) 下载Redis安装包:   
    可以从GitHub上下载Redis的Windows版本，有安装包形式和zip两种选择；  
    
    b) 解压Redis压缩包：  
    将下载的压缩包解压到一个新的文件夹中，例如命名为redis-5.0.10；  
    
    c) 配置Redis：  
    编辑redis.windows.conf文件，设置绑定地址和端口等参数  
    
    d) 启动Redis服务：  
     在命令提示符中定位到解压后的文件夹，输入redis-server.exe redis.windows.conf启动Redis服务；   
     
    e) 测试安装：  
     使用redis-cli.exe -h 127.0.0.1 -p 6379测试是否安装成功；  
     
    f) 注册服务：   
    可以通过命令redis-server --service-install redis.windows.conf将Redis注册为Windows服务，以便自动启动；  
    
    Linux系统下安装Redis  
    
    a) 安装编译工具：  
    确保系统已安装gcc和make等工具，可以通过包管理器安装；  
    
    b) 下载Redis安装包：  
     从Redis官网下载安装包；  
     
     c) 编译安装：  
     解压安装包，使用make命令编译，然后使用sudo make install命令安装；
     
     d ) 启动Redis服务：  
     在终端中输入src/redis-server启动Redis服务；  
     
    e) 查看Redis安装目录：  
    可以使用which redis-server命令查看Redis的安装目录，通常为/usr/local/bin；  
    
      
    f) 配置Redis：  
    编辑/usr/local/bin/redis.conf文件，设置绑定地址和端口等参数；  
    
    g) 测试安装：  
    使用redis-cli命令进入Redis命令行界面，使用set和get命令测试安装是否成功。
   
  - 步骤二 安装RocketMQ  
     rocketMQ 作为一个有节操的程序员，应该也是老生长谈了，就不再赘述，简单说明如下：   
   
   
    下载安装Apache RocketMQ  
     RocketMQ 的安装包分为两种：   
     二进制包和源码包  
      点击如下链接下载 Apache RocketMQ 5.3.1的源码包。  
     二进制包是已经编译完成后可以直接运行的，   源码包是需要编译后运行的  
     
    下载链接：  
     https://dist.apache.org/repos/dist/release/rocketmq/5.3.1/rocketmq-all-5.3.1-source-release.zip  
     解压5.3.1的源码包并编译构建二进制可执行文件
     
     $ unzip rocketmq-all-5.3.1-source-release.zip  
     $ cd rocketmq-all-5.3.1-source-release/  
     $ mvn -Prelease-all -DskipTests -Dspotbugs.skip=true clean install -U  
     $ cd distribution/target/rocketmq-5.3.1/rocketmq-5.3.1    
     
    安装完RocketMQ包后，我们启动NameServer  
    
    ### 启动namesrv  
    $ nohup sh bin/mqnamesrv &  
     
    ### 验证namesrv是否启动成功 
    $ tail -f ~/logs/rocketmqlogs/namesrv.log
    The Name Server boot success...
    
    启动Broker+Proxy
    NameServer成功启动后，我们启动Broker和Proxy。这里我们使用 Local 模式部署， 
    即 Broker 和 Proxy 同进程部署。5.x 版本也支持 Broker 和 Proxy 分离部署以实现更灵活的集群能力。
    详情参考部署教程。
    
    ### 先启动broker
    $ nohup sh bin/mqbroker -n localhost:9876 --enable-proxy &
    
    ### 验证broker是否启动成功, 比如, broker的ip是192.168.1.2 然后名字是broker-a
    $ tail -f ~/logs/rocketmqlogs/proxy.log 
    The broker[broker-a,192.169.1.2:10911] boot success...
    
    在进行工具测试消息收发之前，我们需要告诉客户端NameServer的地址，  
    RocketMQ有多种方式在客户端中设置NameServer地址，这里我们利用环境变量NAMESRV_ADDR
    
    $ export NAMESRV_ADDR=localhost:9876
    $ sh bin/tools.sh org.apache.rocketmq.example.quickstart.Producer
     SendResult [sendStatus=SEND_OK, msgId= ...
    
    $ sh bin/tools.sh org.apache.rocketmq.example.quickstart.Consumer
     ConsumeMessageThread_%d Receive New Messages: [MessageExt...
 
 #### 1.2 配置 
 
   ##### 1.2.1 配置前端
   前端配置主要 vue.config.js  将代理地址配置成你本地的实际后端服务地址  
      
```
  devServer: {
    host: '0.0.0.0',
    port: port,
    open: true,
    proxy: {
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      [process.env.VUE_APP_BASE_API]: {
        target: `http://localhost:8080/smart-socket`,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    },
    disableHostCheck: true
  }

```    

   ##### 1.2.1 配置后端
   主要配置 application.yml 文件   
   如果要使用 Redis 来实现多通道的消息处理、发送，配置如下：  
```
spring:
  redis:
    port: ${SMART_REDIS_PORT:6379}
    host: ${SMART_REDIS_URL:localhost}
    password: ${SMART_REDIS_PWD:}

```   
说明如下：  
- SMART_REDIS_PORT 表示 你本地缓存 端口
- SMART_REDIS_URL 表示 你本地缓存地址
- SMART_REDIS_PWD 表示 你本地缓存密码 
    如果要使用 RocketMQ 来实现多通道的消息处理、发送，配置如下：  
```
rocketmq:
  enabled: false
  topic: SMART_CHAT
  name-server: 192.168.171.128:9876
  # 生产者配置
  producer:
    # 发送同一类消息的设置为同一个group，保证唯一
    group: SMART_CHAT_PRO
    # 发送消息超时时间,默认3000
    sendMessageTimeout: 3000
    # 发送消息失败重试次数，默认2
    retryTimesWhenSendFailed: 2
    # 异步消息重试此处，默认2
    retryTimesWhenSendAsyncFailed: 2
    # 消息最大长度 默认1024*4(4M)
    maxMessageSize: 4096
    # 是否在内部发送失败时重试另一个broker，默认false
    retryNextServer: false
    # 压缩消息阈值，默认4k(1024 * 4)
    compressMessageBodyThreshold: 4096
  consumer:
    group: SMART_CHAT_CON
```    

说明如下：  
- rocketmq.enabled = false 表示默认不启用，要启用请改成true
- rocketmq.name-server 表示要连接的mq服务器地址 
- rocketmq.topic 表示mq消息主题
- 其他配置 老生长谈 不再赘述   

 #### 1.3 运行  
 
  ##### 1.1 运行前端   
  - 步骤一   
  先执行 npm install -g cnpm --registry=https://registry.npmmirror.com/  安装本地依赖  
  - 步骤二   
  再执行 npm run dev 启动本地实例  当然你可以使用IDEA的插件执行  
  - 步骤三   
  启动之后，访问 http://localhost:80/ 如果出现如下图片，表示启动成功   
     ![nvm1](./folder/startui.png)    
    ##### 1.2 前端代码预览   
   - 代码片段一  初始化 WebSocket 服务器  
```
/**
 * 初始化 Websocket
 * 两个参数
 * 昵称账号
 * 所选房间号
 */
CHAT.init = function(nickName,chatRoomId,messageList,obj) {

  if (!nickName || nickName == "undefined" || nickName == "null" ||
    !chatRoomId || chatRoomId == "undefined" || chatRoomId == "null") {

    Message({ message: "聊天出现异常，原因为昵称为空或未选择房间号！", type: 'error', duration: 5 * 1000 })

    Promise.reject('聊天出现异常，原因为昵称为空或未选择房间号' )

    return ;
  }

  // 保存全局参数
  CHAT.NICK_NAME = nickName ;
  CHAT.CHAT_ROOM_ID = chatRoomId ;
  CHAT.messageList = messageList ;
  CHAT.WINDOW = obj ;

 let downloadLoadingInstance = Loading.service({ text: "正在连接服务器，请稍候", spinner: "el-icon-loading", background: "rgba(0, 0, 0, 0.7)", })


  try {

    if(window.WebSocket){

      // 先从状态机中获取
      let target = null ;
      if(store.getters && store.getters.ws ){
          target =  store.getters.ws ;
      }

     if(target && target !=null && target !='undefined'){
       CHAT.SOCKET = target[CHAT.NICK_NAME] ;
       }else {

       CHAT.SOCKET = new WebSocket(CHAT.SERVER_ADDR+"?"+"nickName="+nickName +"&chatRoomId="+chatRoomId ) ;

       // 保存 socket 状态
       let ws = {};

       ws[CHAT.NICK_NAME]= CHAT.SOCKET ;

       store.dispatch('app/SetWs', ws) ;

       console.log(CHAT.SERVER_ADDR+"?"+"nickName="+nickName +"&chatRoomId="+chatRoomId ,'服务器连接地址') ;
     }


      CHAT.SOCKET.onopen = function() {
        Message.info({ message: "恭喜您与服务器连接成功，您使用的房间号为【"+ chatRoomId +']', type: 'success', duration: 5 * 1000 }) ;

        let messageDTO = {
          from: '系统消息',
          to: '全员',
          content: nickName + '上线了',
          msgType: '30',
          channel: 'SYS_CHANNEL',
          sendTime : CHAT.formatDate(new Date())
        }

        CHAT.send(JSON.stringify(messageDTO)) ;

      }

      CHAT.SOCKET.onmessage = function(event ) {

        CHAT.acceptMsg(event.data) ;
      }

      CHAT.SOCKET.onclose = function() {

        if(CHAT.SOCKET == null ){
          return ;
        }

        let messageDTO = {
          from: '系统消息',
          to: '全员',
          content: nickName + '下线了',
          msgType: '30',
          channel: 'SYS_CHANNEL',
          sendTime : CHAT.formatDate(new Date())
        }

        CHAT.send(JSON.stringify(messageDTO)) ;


      }

      CHAT.SOCKET.onerror = function(err ) {

        Message({ message: "WebSocket 服务器异常，原因为："+err, type: 'error', duration: 5 * 1000 })

        downloadLoadingInstance.close() ;
      }

      downloadLoadingInstance.close() ;

    }else {

      Message({ message: "浏览器不支持 WebSocket,请更换Chrome或者FireFox", type: 'error', duration: 5 * 1000 })
      downloadLoadingInstance.close() ;
    }

  }catch (e) {
    downloadLoadingInstance.close() ;
    Message({ message: "与服务器建立连接异常，原因为"+ e, type: 'error', duration: 5 * 1000 })

  }
 
}
```     
- 代码片段二    聊天室房间选择核心逻辑    
```
 this.$refs.registerForm.validate(valid => {
        if (valid) {
          this.loading = true;
          const nickName = this.registerForm.nickName;
          const chatRoomId = this.chatRoomId ;
          this.$alert("<font color='red'> 恭喜您，该系统允许使用 【" + nickName + "】作为昵称 赶快畅聊吧！</font>", '系统提示', {
            dangerouslyUseHTMLString: true,
            type: 'success'
          }).then(() => {
            this.loading = false;
            this.$router.push(
              {
              path : '/chat',
              query: {
              chatRoomId: chatRoomId, nickName: nickName
              }
              }
              );

          }).catch(() => {});
        }
```
- 代码片段三    按房间聊天核心逻辑    
```
     this.loadEmojis() ;
      // 开始连接 webSocket 进行聊天
     let nickName =  this.$route.query.nickName  ;

     this.NICK_NAME = nickName ;

     let chatRoomId =  this.$route.query.chatRoomId  ;

     this.CHAT_ROOM_ID = chatRoomId ;

     CHAT.init(nickName,chatRoomId,this.messageList, this.$refs.chatWindow) ;
     let self = this ;
     // 初始化在线用户
      this.interval =  setInterval(function() {
        getOnlineUsers(chatRoomId).then( res => {
           self.onlineUsers = res.data  ;
          self.onlineRadioUsers = res.data ;
         })
      },2000)

      window.addEventListener("keydown", this.keyDowns);
```    
  ##### 1.3 运行后端   
   - 步骤一   运行 StartApplication 运行成功 将会打印如下日志   
   ```
2024-12-06 11:22:19.500  INFO 10332 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2024-12-06 11:22:19.513  INFO 10332 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2024-12-06 11:22:19.513  INFO 10332 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.46]
2024-12-06 11:22:19.615  INFO 10332 --- [           main] o.a.c.c.C.[.[localhost].[/smart-chatRoom]  : Initializing Spring embedded WebApplicationContext
2024-12-06 11:22:19.616  INFO 10332 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1798 ms
2024-12-06 11:22:19.663  INFO 10332 --- [           main] o.a.c.c.C.[.[localhost].[/smart-chatRoom]  : Set web app root system property: 'webapp.root' = [C:\Users\yuenbin\AppData\Local\Temp\tomcat-docbase.8080.3130202185464995042\]
2024-12-06 11:22:19.694  INFO 10332 --- [           main] c.y.s.config.RocketMQTemplateConfig      : rocketMQ starts ....
2024-12-06 11:22:21.235  INFO 10332 --- [           main] o.a.r.s.a.RocketMQAutoConfiguration      : a producer (SMART_CHAT_PRO) init on namesrv 192.168.171.128:9876
2024-12-06 11:22:23.282  INFO 10332 --- [           main] o.a.r.s.a.ExtProducerResetConfiguration  : Set real producer to :rocketMQTemplateConfig 
2024-12-06 11:22:23.313  INFO 10332 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path '/smart-chatRoom'
```
   - 步骤二 将上述 http://localhost:8080/smart-chatRoom 地址填充到 前端 代理服务器中 
   
  ##### 1.4 后端代码预览
- 代码片段一   rocketMQ 按通道发送消息  
```
/**
     * @Description： 一号房间 通道
     * @author : yuenbin
     * @date： 17:04 2024/12/3
     * @Motto： It is better to be clear than to be clever !
     **/
    @EventListener(condition = "#event.msgTag=='" + TagConstant.TAG_CHANNEL_01+"'")
    public void sendMsgByChannel01(BaseEvent event) {
        MessageDTO messageDTO = (MessageDTO) event.getSource();
        log.info("事件监听类: tag: {}, msgType: {}, date: {}, data:{}", event.getMsgTag(), event.getMsgType(), event.getDate(), event.getSource());
        webSocketServer.sendMsg(messageDTO);
    }
```
  - 代码片段二   rocketMQ 暗通道处理消息
```
   @Override
    public void onMessage(MessageExt message) {
        String topic = message.getTopic();
        String tag = message.getTags();
        byte[] body = message.getBody();
        String keys = message.getKeys();
        String msgId = message.getMsgId();
        String realTopic = message.getProperty("REAL_TOPIC");
        String originMessageId = message.getProperty("ORIGIN_MESSAGE_ID");

        // 获取重试的次数 失败一次消息中的失败次数会累加一次
        int reconsumeTimes = message.getReconsumeTimes();

        String jsonBody = JackJsonUtil.toJSONString((new String(body)));
        log.info("消息监听类: msgId:{},topic:{}, tag:{}, body:{},keys:{},realTopic:{},originMessageId:{},reconsumeTimes:{}", msgId, topic, tag, jsonBody, keys, realTopic, originMessageId, reconsumeTimes);

        // 布隆过滤器进行去重
//        if (bitMapBloomFilter.contains(keys)) {
//            return;
//        }
//        bitMapBloomFilter.add(keys);

        applicationContext.publishEvent(new BaseEvent(tag, jsonBody));
    }
```  

- 代码片段三  WebSocket 消息按通道发送消息
```
   // 发送普通聊天消息
    public void sendMsg (MessageDTO messageDTO){
        String msg = JSON.toJSONString(messageDTO,SerializerFeature.WriteMapNullValue) ;
        String to = messageDTO.getTo();
        String from = messageDTO.getFrom();
        // 兼容群发情况
        if(to.equalsIgnoreCase("ALL")){
            sendBroadCastMsg(messageDTO);
        }else {

            if(USER_CACHE.containsKey(to)){
                UserDTO userDTO = USER_CACHE.get(to);
                if(wsSessionManger.getSessionPools().containsKey(userDTO)){
                    Session session = wsSessionManger.getSessionPools().get(userDTO);
                    sendMessage(session,msg);
                }
            }else {
                // 判断当前要聊天的用户是否在线，如果不在线那么就发送一条离线提醒
                Session session = wsSessionManger.getSessionPools().get(USER_CACHE.get(from));
                messageDTO.setContent("当前已离线");
                messageDTO.setFrom(to);
                messageDTO.setTo(from);
                sendSysMsg(session,messageDTO);
            }
        }
    }
```
- 代码片段四  Redis 按通道监听消息    
```
    public void onMessage(Message message, byte[] bytes) {
        // 获取通道编码
        String channel = new String(message.getChannel()) ;
        // 获取消息内容
        String body = new String(message.getBody(),Charset.defaultCharset());

        MessageDTO messageDTO = JSONObject.parseObject(body, MessageDTO.class, SerializerFeature.WRITE_MAP_NULL_FEATURES);
        log.info("当前获取到的Redis订阅通道为：{}，消息为：{}",channel, JSON.toJSONString(messageDTO));
        if(messageDTO.getChannel().equalsIgnoreCase(channel)){
            webSocketServer.sendMsg(messageDTO);
        }
    }
```
- 代码片段五 redis 设置房间通道核心逻辑 
```
  public RedisMessageListenerContainer redisMessageListenerContainer (RedisConnectionFactory factory,
                                                                         RedisMessageListener redisMessageListener ){
        RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer() ;
        redisMessageListenerContainer.setConnectionFactory(factory);

        List topics = new LinkedList() ;

        Consumer<Topic> consumer = topics ::add;

        Topic CHAT_ROOM_N_01 = new ChannelTopic(Constants.CHAT_ROOM_N_01) ;
        consumer.accept(CHAT_ROOM_N_01);
        Topic CHAT_ROOM_N_02 = new ChannelTopic(Constants.CHAT_ROOM_N_02) ;
        consumer.accept(CHAT_ROOM_N_02);
        Topic CHAT_ROOM_N_03 = new ChannelTopic(Constants.CHAT_ROOM_N_03) ;
        consumer.accept(CHAT_ROOM_N_03);

        redisMessageListenerContainer.addMessageListener(redisMessageListener,topics);
        return redisMessageListenerContainer ;

```
  #### 1.4 访问   
  
  <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/diy.gif" width="1000px" height="400px" />
  
  - 步骤一 启动前端之后，页面自动跳入到连接选房间页   
  ![chat](./folder/c01.png)    
  点确定 进入下一页   
  ![chat](./folder/c011.png)     
   - 步骤二 选择要聊天房间号，点击立即进入聊天，打开如下图：     
   ![chat](./folder/c02.png)   
   - 步骤三 重复步骤一、步骤二，多造几个聊天对象     
   ![chat](./folder/c021.png)    
   ![chat](./folder/c022.png)    
   - 步骤四 聊天建立好之后，进入到聊天室 ，由于大家选择的是一号房间，所以能看到所有在线用户  ，当前  
   一号房间内聊天在线用户为3人   
     ![chat](./folder/c04.png)    
   - 步骤五 为突出对比性，再上线一个二号房间的用户   ，比如 张无忌选择二号房间聊天室   
       ![chat](./folder/c05.png)    
       发现 当前聊天室内（二号房间）除了张无忌本人，再无其他上线用户，做到了隔离  
       ![chat](./folder/c051.png)    
       切回至 一号聊天室 ，发现一号聊天室还是只有 张三、李四、王五三人，并没有 张无忌  
       ![chat](./folder/c052.png)    
       
   - 步骤六 切回聊天室一号房间，随便选择一个用户聊天，发现上线用户只有三人 没有二号房间的 张无忌 ，比如 选择张三     
        ![chat](./folder/c06.png)     
        输入消息内容，选择 聊天模式 
   - 步骤七 选择单聊模式  比如 选择王五，点确定 表示 要跟王五单聊
           ![chat](./folder/c07.png)     
           ![chat](./folder/c08.png)     
           ![chat](./folder/c081.png)     
   - 步骤八 选择群聊模式   
             ![chat](./folder/c09.png)      
         张三 群发消息   
             ![chat](./folder/c091.png)    
         切到 李四   能看到张三发的群消息
             ![chat](./folder/c092.png)    
         切到 王五 能看到张三发的群消息
             ![chat](./folder/c093.png)    
        切到 王五 能看到张三发的群消息 但 张三与王五的单聊消息 ，别人看不到
             ![chat](./folder/c094.png)     
             
   - 步骤九 切到 二号聊天室，发现 张无忌并没有收到 张三发来的群发，说明 群聊成功
               ![chat](./folder/c10.png)    
                   
##   <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/科技.png" width="25px" height="25px" /> 安装教程
  适用于打包到服务器，通过客户端或集成方式运行的场景   
1.  后端Docker 部署
```
# 使用Java 8镜像作为基础镜像
FROM openjdk8:1.0.3

# 将构建产生的jar文件复制到容器中
ADD smart-chatRoom-SNAPSHOT.jar smart-chatRoom.jar

# 设置环境变量，用于配置Spring Boot应用
ENV APP_NAME=smart-chatRoom SW_AGENT_NAME=smart-chatRoom
ENV LOG_PATH=/opt/logs
# 指定日志挂载点
VOLUME ["/app/logs", "/opt/logs"]

# 设置容器启动时执行的命令
CMD ["java","-jar", "/smart-chatRoom.jar"]

```
2.  前端UI部署
```
FROM  nginx:latest
#维护者信息
COPY nginx.conf /etc/nginx/nginx.conf
COPY index.html /opt/apps/htdocs/index.html
COPY ./dist /apps/smart-chatRoom
EXPOSE 80
#指定挂载点
VOLUME ["/var/log/nginx"]

```
##    <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/自然风景.png" width="25px" height="25px" /> 后续计划

1.  接入OSS对象存储能力，接收非结构化文档存储
2.  支持聊天的时候，发送图片，并将图片发布到OSS云服务
3.  支持聊天内容的云端存储，支持离线记录的恢复
4.  支持消息的撤回，参考微信、QQ消息，实践IM消息的及时通讯
5.  支持消息群公告的发布，允许群发或单发公告，并提醒
6.  优化群聊成员管理，实时展示在线、离线状态
6.  支持群聊成员级别、身份维护、标签展示
6.  支持聊天文件传递
6.  支持聊天发红包
......

 
##    <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/风景名胜.png" width="25px" height="25px" /> 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
 
##    <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/山脉风景.png" width="25px" height="25px" /> 参考资料

- Smart Kettle之后端介绍：https://my.oschina.net/yaukie/blog/4973081   
- Smart Kettle之新品推介：https://my.oschina.net/yaukie/blog/4969927   
- Vue 那点事儿：https://my.oschina.net/yaukie/blog/1547678  
- Docker 那点事儿：https://my.oschina.net/yaukie/blog/3165074  


##    <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/再生科技.png" width="25px" height="25px" /> 捐赠
开源不易，感谢捐赠  
No matter how much the donation amount is enough to express your thought, thank you very much ：）   
  [To donate](https://my.oschina.net/yaukie/blog/4968854)
无论捐赠金额多少都足够表达您这份心意，非常感谢 ：）    
  [前往捐赠](https://my.oschina.net/yaukie/blog/4968854)  
  
  ##    <img src="https://gitee.com/yaukie/smart-chatRoom/raw/master/folder/科技4.png" width="25px" height="25px" /> 作者简介
 @Author: yuenbin  
        屌丝一枚，码农搬运工，正努力成为互联网行业的土豪，多多益善，来者不拒！  
        佛祖保佑捐赠这些人写程序永无bug，工资翻倍，迎娶白富美，走上人生巅峰！  
       汉化处理： JAVA_OPTS="-Duser.language=zh -Duser.region=CN -Dfile.encoding=UTF-8"
       
![支付宝](https://gitee.com/yaukie/smartkettle/raw/master/folder/wechat.jpg)  

个人公众号    

![个人公众号](https://gitee.com/yaukie/smartkettle/raw/master/folder/qrcode_for_gh_e44563e3338c_860.jpg)
