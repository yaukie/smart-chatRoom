package com.yaukie.socket.config;

import com.yaukie.socket.manager.WsSessionManger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WsSessionConfig {

    @Bean
    public WsSessionManger wsSessionManger (){
        return new WsSessionManger() ;
    }
}
