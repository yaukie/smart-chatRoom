package com.yaukie.socket.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.ExtRocketMQTemplateConfiguration;
import org.apache.rocketmq.spring.core.RocketMQTemplate;

import javax.annotation.PostConstruct;

@Slf4j
@ExtRocketMQTemplateConfiguration(nameServer = "${rocketmq.name-server")
public class RocketMQTemplateConfig  extends RocketMQTemplate  {

    @PostConstruct
    public void init (){
        log.info("rocketMQ starts ....");
    }

}
