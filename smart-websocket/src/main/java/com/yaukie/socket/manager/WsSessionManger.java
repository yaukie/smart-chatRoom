package com.yaukie.socket.manager;

import com.yaukie.socket.dto.UserDTO;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * @功能名称： webSocketSession 管理器
 * @功能描述： webSocketSession 管理器
 * @作者： yuenbin
 * @创建时间： 22:00 2024/11/25
 * @Motto： It is better to be clear than to be clever !
 **/
public class WsSessionManger {

    /***用户存储 session 对应关系*/
    private  Map<UserDTO, Session> SESSION_POOLS_ = new ConcurrentHashMap<>() ;

    /***用于 session 实时统计*/
    private  AtomicInteger SESSIONS_ = new AtomicInteger(0) ;


    public  int removeSession (UserDTO userDTO){
        SESSION_POOLS_.remove(userDTO) ;
        return SESSIONS_.decrementAndGet() ;
    }


    public  int addSession(UserDTO userDTO,Session session){
        if(!SESSION_POOLS_.containsKey(userDTO)){
            SESSION_POOLS_.put(userDTO,session) ;
            return SESSIONS_.incrementAndGet() ;
        }
        return  0 ;
    }

    public  void closeSession (Session session){
        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Description： 查询 当前在线用户
     * @author : yuenbin
     * @date： 11:36 2024/11/26
     * @Motto： It is better to be clear than to be clever !
     **/
    public  List<UserDTO> getOnlineUsers (){
        List onlineUsers =new ArrayList() ;
        if(!CollectionUtils.isEmpty(SESSION_POOLS_)){
            Consumer<UserDTO> add = onlineUsers::add;
            SESSION_POOLS_.forEach((k,v) ->{
                add.accept(k);
            });
        }

        return onlineUsers ;
    }

    public  Map<UserDTO,Session> getSessionPools (){
        return SESSION_POOLS_ ;
    }

}

