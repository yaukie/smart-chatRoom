package com.yaukie.socket.listener;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.yaukie.socket.WebSocketServer;
import com.yaukie.socket.dto.MessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import java.nio.charset.Charset;

/**
 * @功能名称：  编写一个Redis 监听器，用于监听redis通道信息
 * @功能描述：  编写一个Redis 监听器，用于监听redis通道信息
 * @作者： yuenbin
 * @创建时间： 21:41 2024/11/25
 * @Motto： It is better to be clear than to be clever !
 **/
@Slf4j
public class RedisMessageListener  extends MessageListenerAdapter {

    @Autowired
    private WebSocketServer webSocketServer ;

    @Override
    public void onMessage(Message message, byte[] bytes) {
        // 获取通道编码
        String channel = new String(message.getChannel()) ;
        // 获取消息内容
        String body = new String(message.getBody(),Charset.defaultCharset());

        MessageDTO messageDTO = JSONObject.parseObject(body, MessageDTO.class, SerializerFeature.WRITE_MAP_NULL_FEATURES);
        log.info("当前获取到的Redis订阅通道为：{}，消息为：{}",channel, JSON.toJSONString(messageDTO));
        if(messageDTO.getChannel().equalsIgnoreCase(channel)){
            webSocketServer.sendMsg(messageDTO);
        }

    }

}
