package com.yaukie.socket.constants;

public interface Constants {

    /** 一号聊天房间 */
    public static final String CHAT_ROOM_N_01 = "CHAT_ROOM_N_01" ;

    /** 二号聊天房间 */
    public static final String CHAT_ROOM_N_02 = "CHAT_ROOM_N_02" ;

    /** 三号聊天房间 */
    public static final String CHAT_ROOM_N_03= "CHAT_ROOM_N_03" ;

    /***系统 默认通道 */
    public static final String SYS_CHANNEL= "SYS_CHANNEL" ;

    /**广播通道*/
    public static final String BROAD_CAST_CHANNEL= "BROAD_CAST_CHANNEL" ;


    public static   enum MsgType {

        NORMAL("10","普通消息"),
        BROAD("20","广播消息"),
        SYS("30","系统消息");

        private String code ;

        private String desc ;

        private MsgType(String code,String desc){
            this.code = code ;
            this.desc = desc ;
        }


        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }


}
