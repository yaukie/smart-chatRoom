
package com.yaukie.socket.constants;
/**
  * @功能名称：OperationResultConstant
  * @功能描述：TODO
  * @作者：yuenbin
  * @创建时间：2022/3/6 12:53
  * @Motto：It is better to be clear than to be clever !
**/
public enum BaseResultConstant {
    SUCCESS(200, "成功"),
    FAILED(0, "失败"),
    UNKNOWN_EXCEPTION(10001, "未捕获的异常");

    public int code;
    public String msg;

    private BaseResultConstant(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return this.msg;
    }

    public void setMessage(String msg) {
        this.msg = msg;
    }
}
