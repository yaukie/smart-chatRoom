package com.yaukie.socket.constants;

 
/**
  * @功能名称：BaseResult
  * @功能描述：TODO
  * @作者：yuenbin
  * @创建时间：2022/3/6 12:53
  * @Motto：It is better to be clear than to be clever !
**/
public class BaseResult<T> {
     public int code;
     public String msg;
     public T data;

    public BaseResult() {
        this.code = BaseResultConstant.SUCCESS.code;
        this.msg = BaseResultConstant.SUCCESS.msg;
        this.data = null;
    }

    public BaseResult(int code, String msg) {
        this(code, msg, null);
    }

    public BaseResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;

    }


    public BaseResult(BaseResultConstant ocResultConstant, T data) {
        this(ocResultConstant.code, ocResultConstant.msg, data);
    }

    public void setApiResultConstant(BaseResultConstant ocResultConstant) {
        this.code = ocResultConstant.code;
        this.msg = ocResultConstant.msg;
     }

    public void setApiResultConstant(int code, String msg) {
        this.code = code;
        this.msg = msg;
     }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
     }

    public String getmsg() {
        return this.msg;
    }

    public void setmsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public String toString() {
        return "ApiResult{code=" + this.code + ", msg='" + this.msg + '\'' + ", data=" + this.data + ", success"+'}';
    }
}
