package com.yaukie.socket.controller;


import com.yaukie.socket.constants.BaseResult;
import com.yaukie.socket.constants.BaseResultConstant;
import com.yaukie.socket.dto.UserDTO;
import com.yaukie.socket.manager.WsSessionManger;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@RestController
public class MessageController    {

    @Autowired
    private WsSessionManger wsSessionManger ;

    @GetMapping("/getOnlinerUsers/{chatRoomId}")
    @ResponseBody
      public Object sendSocket(
            @PathVariable(value = "chatRoomId") String chatRoomId
      ) {

        List<UserDTO> onlineUsers = wsSessionManger.getOnlineUsers();
        List<UserDTO> finalList = new ArrayList<>();
        if(!CollectionUtils.isEmpty(onlineUsers)){
            finalList = onlineUsers.stream().filter(userDTO -> userDTO.getChatRoomId().equalsIgnoreCase(chatRoomId)).collect(Collectors.toList());
        }
        return new BaseResult(BaseResultConstant.SUCCESS,finalList) ;
    }

}
