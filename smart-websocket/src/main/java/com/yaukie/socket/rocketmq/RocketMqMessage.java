package com.yaukie.socket.rocketmq;

import lombok.Data;

import java.util.Date;

@Data
public class RocketMqMessage {

    private String msgId;
     private String msgTopic;
     private String msgTag;
     private String msgKeys;
     private String msgBody;
     private String msgType;
     private String msgRetryId;
     private String msgRetryTopic;
     private Date createTime;
}
