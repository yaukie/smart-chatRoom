package com.yaukie.socket.rocketmq;

/**
 * @功能名称： 指定消息通道
 * @功能描述： 指定消息通道
 * @作者： yuenbin
 * @创建时间： 17:03 2024/12/3
 * @Motto： It is better to be clear than to be clever !
 **/
public interface TagConstant {

    public static final  String TAG_CHANNEL_01 = "SMART_CHAT_01";

    public static final  String TAG_CHANNEL_02= "SMART_CHAT_02";

    public static final  String TAG_CHANNEL_03= "SMART_CHAT_03";
}
