package com.yaukie.socket.rocketmq;

/**
 * @功能名称： rocketMQ 基础配置
 * @功能描述： rocketMQ 基础配置
 * @作者： yuenbin
 * @创建时间： 17:00 2024/12/3
 * @Motto： It is better to be clear than to be clever !
 **/
public interface MqConstant {

    public static  final String NAME_SERVER_ADDRESS = "localhost:9876";

    public static  final  String TOPIC = "SMART_CHAT";

    public static final  String HEAP_TOPIC = "heap-up-topic";

}

