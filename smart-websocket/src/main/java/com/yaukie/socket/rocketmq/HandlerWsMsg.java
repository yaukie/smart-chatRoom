package com.yaukie.socket.rocketmq;

import com.yaukie.socket.WebSocketServer;
import com.yaukie.socket.dto.MessageDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @功能名称： 真正用来发送客户端发送来的消息
 * @功能描述： 真正用来发送客户端发送来的消息
 * @作者： yuenbin
 * @创建时间： 17:00 2024/12/3
 * @Motto： It is better to be clear than to be clever !
 **/
@Component
@Slf4j
public class HandlerWsMsg {

    @Autowired
    private WebSocketServer webSocketServer ;

    /**
     * @Description： 一号房间 通道
     * @author : yuenbin
     * @date： 17:04 2024/12/3
     * @Motto： It is better to be clear than to be clever !
     **/
    @EventListener(condition = "#event.msgTag=='" + TagConstant.TAG_CHANNEL_01+"'")
    public void sendMsgByChannel01(BaseEvent event) {
        MessageDTO messageDTO = (MessageDTO) event.getSource();
        log.info("事件监听类: tag: {}, msgType: {}, date: {}, data:{}", event.getMsgTag(), event.getMsgType(), event.getDate(), event.getSource());
        webSocketServer.sendMsg(messageDTO);
    }

    /**
     * @Description： 二号房间 通道
     * @author : yuenbin
     * @date： 17:04 2024/12/3
     * @Motto： It is better to be clear than to be clever !
     **/
    @EventListener(condition = "#event.msgTag=='" + TagConstant.TAG_CHANNEL_02 +"'")
    public void sendMsgByChannel02(BaseEvent event) {
        MessageDTO messageDTO = (MessageDTO) event.getSource();
        log.info("事件监听类: tag: {}, msgType: {}, date: {}, data:{}", event.getMsgTag(), event.getMsgType(), event.getDate(), event.getSource());
        webSocketServer.sendMsg(messageDTO);
    }

    /**
     * @Description： 三号房间 通道
     * @author : yuenbin
     * @date： 17:04 2024/12/3
     * @Motto： It is better to be clear than to be clever !
     **/
    @EventListener(condition = "#event.msgTag=='" + TagConstant.TAG_CHANNEL_03 +"'")
    public void sendMsgByChannel03(BaseEvent event) {
        MessageDTO messageDTO = (MessageDTO) event.getSource();
        log.info("事件监听类: tag: {}, msgType: {}, date: {}, data:{}", event.getMsgTag(), event.getMsgType(), event.getDate(), event.getSource());
        webSocketServer.sendMsg(messageDTO);
    }

}
