package com.yaukie.socket.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDTO implements Serializable {

    private String userId ;

    private String nickName;

    private String headPic ;

    private String chatRoomId ;

}
