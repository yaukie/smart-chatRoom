package com.yaukie.socket.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @功能名称： 构建消息主体
 * @功能描述： 构建消息主体
 * @作者： yuenbin
 * @创建时间： 9:11 2024/11/26
 * @Motto： It is better to be clear than to be clever !
 **/
@Data
public class MessageDTO  implements Serializable {

    // 消息来源
    private String from ;

    // 消息目标
    private String to ;

    // 消息内容
    private String content ;

    // 消息类型  0 普通消息 -1 系统群发消息 -2 广播群发消息
    private String msgType ;

    // 选择的哪个通道（房间号）
    private String channel  ;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private String sendTime ;

}
